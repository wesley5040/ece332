# ece332

"Projects" for ECE312 at Rose-Hulman

# Project 1
Just a useless chat program that echos messages back and forth.  Use `make` to build after setting the server IP address in the client source code.  Then run `./server` to start the server and `./client` to start the client.  Use `^C` to exit.

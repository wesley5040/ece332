/* UDP sender/reciver for Project 2 in ECE312 Winter 2018/19
 *
 * This is mostly the client example provided, but instead of sending a
 * pre-defined message over UDP to a server, then printing out the response, it
 * gets the message to send from STDIN and puts the response in STDOUT.
 *
 * See the Makefile for usage examples.  This is designed to be used using UNIX
 * pipes
 *
 * Last modified by Wesley Van Pelt, Feb 2019
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define SERVER  "137.112.41.52"
#define PORT    1874
#define BUFSIZE 16384

int main()
{
	int clientSocket, nBytes, i;
	char buffer[BUFSIZE];
	struct sockaddr_in clientAddr, serverAddr;

	/*Create UDP socket*/
	if ((clientSocket = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("cannot create socket");
		return 0;
	}

	/* Bind to an arbitrary return address.
	 * Because this is the client side, we don't care about the address
	 * since no application will initiate communication here - it will
	 * just send responses
	 * INADDR_ANY is the IP address; 0 is the port (allow OS to select port)
	 * htonl converts a long int (e.g. address) to a network representation
	 * htons converts a short int (e.g. port) to a network representation */
	memset((char *) &clientAddr, 0, sizeof (clientAddr));
	clientAddr.sin_family = AF_INET;
	clientAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	clientAddr.sin_port = htons(0);

	if (bind(clientSocket,
		 (struct sockaddr *) &clientAddr,
		 sizeof (clientAddr)) < 0) {
		perror("bind failed");
		return 0;
	}

	/* Configure settings in server address struct */
	memset((char*) &serverAddr, 0, sizeof (serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr(SERVER);
	memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);

	// Get and send the data
	uint8_t buf[BUFSIZE];
	int b = read(STDIN_FILENO, buf, BUFSIZE);
	if (sendto(clientSocket, buf, b, 0,
		   (struct sockaddr *) &serverAddr,
		   sizeof (serverAddr)) < 0) {
		perror("sendto failed");
		return 0;
	}
	nBytes = recvfrom(clientSocket, buf, BUFSIZE, 0, NULL, NULL);
	close(clientSocket);

	write(STDOUT_FILENO, buf, nBytes);
	return 0;
}

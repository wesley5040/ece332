/* RHMP encapsulator/de-encapsulator for Project 2 in ECE312 Winter 2018/19
 *
 * Puts a payload read from STDIN inside an RHMP packet and writes it to STDOUT.
 * If you try to give it a payload that is bigger than (BUFLEN - 3), it will be
 * truncated.
 *
 * As with udp.c and rhp.c, it is designed to be used with UNIX pipes.  This
 * program can take 0 or 1 arguments.  If none are given, it will try to
 * de-encapsulate the given payload.  If there is one starting with an 's', then
 * it will encapsulate the given payload.  For the RHMP packet to be of the type
 * Reserved message, give a '1' after the 's', for it to be of type ID_Request,
 * use '2', for ID_Response use '4', for Message_Request use '8', and for
 * Message_Response use 'G' (one letter higher than F, which is 15).
 *
 * Example usage requesting a message:
 *
 * # ./rhmp s8 | ./rhp s0 | ./udp | ./rhp | ./rhmp | cat
 *
 * Note 0:  Packet details print to stderr so it doesn't interfere with all the
 *          piping that's going on
 * Note 1:  The 32-bit ID recived from ID_Response will be sent down the pipe in
 *          binary format, but it will be printed to the packet info output in
 *          hex
 * Note 2:  If the message type doesn't have a payload, this will not read
 *          anything from STDIN
 *
 * Created by Wesley Van Pelt, Feb 2019
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#define BUFLEN 2048
#define CID    312



// Pipe stuff from STDIN down the line encapsulated in RHMP
void snd(char type)
{
	uint8_t buf[BUFLEN];
	int     i, b;

	// Figure out and populate header
	if (type == '4' || type == 'G') {
		b = read(STDIN_FILENO, buf, BUFLEN);

		// Move all the stuff over by 3 bytes to make room for header
		if (b > BUFLEN - 3)
			b = BUFLEN - 3;
		for (i = 0; i < b; i++)
			buf[i + 3] = buf[i];
		buf[2] = b;
	} else {
		buf[2] = 0x00;
	}
	// This is after the if-else above since it might move data around
	buf[0] = (CID << 6) | (type - 0x30);
	buf[1] = (CID >> 2);

	// Send the encapsulated data down the pipe
	write(STDOUT_FILENO, buf, b + 3);
}


// Strips off RHMP stuff and pipes it down the line
void rcv()
{
	uint8_t buf[BUFLEN];

	int b = read(STDIN_FILENO, buf, BUFLEN);
	if (b == 0)
		return;

	fprintf(stderr, "\nRHMP:\n");
	fprintf(stderr, "    Type:    %d\n", buf[0] & 0x3F);
	fprintf(stderr, "    CommID:  %d\n", ((buf[0]&0xC0)>>6)+(buf[1]<<2));
	fprintf(stderr, "    Length:  %d\n", buf[2]);
	fprintf(stderr, "    Payload: %s\n", ((buf[0]&0x3F)==4)?"<ID>":buf+3);

	write(STDOUT_FILENO, buf + 3, b - 3);
}


int main(int argc, char** argv)
{
	if (argc > 1 && argv[1][0] == 's')
		snd(argv[1][1]);
	else
		rcv();

	return 0;
}

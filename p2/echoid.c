/* ID echoer for Project 2 in ECE312 Winter 2018/19
 *
 * Simply takes in a 32-bit unsigned int over stdin and echos it in hex
 *
 * Example usage requesting an ID:
 *
 * # ./rhmp s2 | ./rhp s0 | ./udp | ./rhp | ./rhmp | ./echoid
 *
 * Created by Wesley Van Pelt, Feb 2019
 */

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

int main()
{
	uint8_t buf[4];
	int b = read(STDIN_FILENO, buf, 4);
	if (b == 4)
		printf("\nID:  0x%X%X%X%X\n", buf[3], buf[2], buf[1], buf[0]);
	return 0;
}

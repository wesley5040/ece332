/************* UDP CLIENT CODE *******************/

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>

#include "badstuff.c"

#define SERVER "137.112.41.52"
#define MESSAGE "hello there"
#define PORT 1874
#define BUFSIZE 16384

int main() {
	int clientSocket, nBytes, i;
	char buffer[BUFSIZE];
	struct sockaddr_in clientAddr, serverAddr;

	/*Create UDP socket*/
	if ((clientSocket = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("cannot create socket");
		return 0;
	}

	/* Bind to an arbitrary return address.
	 * Because this is the client side, we don't care about the address
	 * since no application will initiate communication here - it will
	 * just send responses
	 * INADDR_ANY is the IP address and 0 is the port (allow OS to select port)
	 * htonl converts a long integer (e.g. address) to a network representation
	 * htons converts a short integer (e.g. port) to a network representation */
	memset((char *) &clientAddr, 0, sizeof (clientAddr));
	clientAddr.sin_family = AF_INET;
	clientAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	clientAddr.sin_port = htons(0);

	if (bind(clientSocket, (struct sockaddr *) &clientAddr, sizeof (clientAddr)) < 0) {
		perror("bind failed");
		return 0;
	}

	/* Configure settings in server address struct */
	memset((char*) &serverAddr, 0, sizeof (serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr(SERVER);
	memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);


	// MSG1 ////////////////////////////////////////////////////////////////
	uint8_t msg[14];
	msg[0]  = 0x01; // Type 1
	msg[1]  = 0x06; // Payload len of 6
	msg[2]  = 0x00;
	msg[3]  = 0xD9; // CM3289
	msg[4]  = 0x0C;
	msg[5]  = 'h';
	msg[6]  = 'e';
	msg[7]  = 'l';
	msg[8]  = 'l';
	msg[9]  = 'o';
	msg[10] = '\0';
	msg[11] = 0x00;
	uint16_t chk = calc_ichksum(msg, 12);
	msg[12] = chk & 0xFF;
	msg[13] = chk >> 8;

	if (sendto(clientSocket, msg, sizeof(msg), 0,
			(struct sockaddr *) &serverAddr, sizeof (serverAddr)) < 0) {
		perror("sendto failed");
		return 0;
	}
	nBytes = recvfrom(clientSocket, buffer, BUFSIZE, 0, NULL, NULL);
	printf("Received from server (%d bytes)\n", nBytes);

	// for (i = 0; i < BUFSIZE; i++) {
	// 	printf("%c", ((char*) buffer)[i]);
	// }
	// printf("\n\n\n");

	print_RHP((uint8_t *) &buffer, nBytes);
	printf("\n\n\n");


	// MSG 2 ///////////////////////////////////////////////////////////////
	uint8_t msg2[10];
	// RHP
	msg2[0] = 0x00; // Type 0
	msg2[1] = 0x69; // CM105
	msg2[2] = 0x00;
	msg2[3] = 0xD9; // CM3289
	msg2[4] = 0x0C;
	// RHMP
	// 0b001000     -> type 8
	// 0b0100111000 -> commID 312
	// c1 c0 t5 t4 t3 t2 t1 t0 c9 c8 c7 c6 c5 c4 c3 c2
	msg2[5] = 0x08;
	msg2[6] = 0x4E;
	msg2[7] = 0x00; // payload len = 0
	// RHP
	chk = calc_ichksum(msg2, 8);
	msg2[8] = chk & 0xFF;
	msg2[9] = chk >> 8;

	if (sendto(clientSocket, msg2, sizeof(msg2), 0,
			(struct sockaddr *) &serverAddr, sizeof (serverAddr)) < 0) {
		perror("sendto failed");
		return 0;
	}
	nBytes = recvfrom(clientSocket, buffer, BUFSIZE, 0, NULL, NULL);
	printf("Received from server (%d bytes)\n", nBytes);

	// for (i = 0; i < BUFSIZE; i++) {
	// 	printf("%c", ((char*) buffer)[i]);
	// }
	// printf("\n\n\n");

	print_RHP((uint8_t *) &buffer, nBytes);
	printf("\n\n\n");


	// MSG 3 ///////////////////////////////////////////////////////////////
	uint8_t msg3[10];
	// RHP
	msg3[0] = 0x00; // Type 0
	msg3[1] = 0x69; // CM105
	msg3[2] = 0x00;
	msg3[3] = 0xD9; // CM3289
	msg3[4] = 0x0C;
	// RHMP
	// 0b001000     -> type 2
	// 0b0100111000 -> commID 312
	// c1 c0 t5 t4 t3 t2 t1 t0 c9 c8 c7 c6 c5 c4 c3 c2
	msg3[5] = 0x02;
	msg3[6] = 0x4E;
	msg3[7] = 0x00; // payload len = 0
	// RHP
	chk = calc_ichksum(msg3, 8);
	msg3[8] = chk & 0xFF;
	msg3[9] = chk >> 8;

	if (sendto(clientSocket, msg3, sizeof(msg3), 0,
			(struct sockaddr *) &serverAddr, sizeof (serverAddr)) < 0) {
		perror("sendto failed");
		return 0;
	}
	nBytes = recvfrom(clientSocket, buffer, BUFSIZE, 0, NULL, NULL);
	printf("Received from server (%d bytes)\n", nBytes);

	// for (i = 0; i < BUFSIZE; i++) {
	// 	printf("%c", ((char*) buffer)[i]);
	// }
	// printf("\n\n\n");

	print_RHP((uint8_t *) &buffer, nBytes);
	printf("\n\n\n");

	close(clientSocket);
	return 0;
}

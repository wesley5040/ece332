uint16_t calc_ichksum(uint8_t* buffer, int len) {
	uint32_t sum   = 0;
	uint16_t sum16 = 0;
	int i;

	// First summing
	uint16_t *buf = (uint16_t *) buffer;
	for (i = 0; i < len / 2; sum += buf[i++]);

	// Check for the extra odd byte and add it on
	if (i * 2 < len)
		sum += ((uint16_t) buffer[i * 2]) << 8;

	// Add those bits over 16 back into the sum, invert, and return
	return ~((sum & 0xFFFF) + (sum >> 16));
}


void print_RHMP(uint8_t* buf) {
	printf("RHMP:\n");
	printf("    Type:    %d\n", buf[0] & 0x3F);
	printf("    CommID:  %d\n", ((buf[0] & 0xC0) >> 6) + (buf[1] << 2));
	printf("    Length:  %d\n", buf[2]);
	switch (buf[0] & 0x3F) {
	case 4:
		printf("    Payload: 0x%X%X%X%X\n", buf[6], buf[5], buf[4], buf[3]);
		break;
	default:
		printf("    Payload: %s\n", buf + 3);
		break;
	}
}


void print_RHP(uint8_t* buf, int len) {
	printf("RHP:\n");
	printf("    Type:         %d\n", buf[0]);
	printf("    Des Port/Len: %d\n", buf[1] + (buf[2] >> 8));
	printf("    Src Port:     %d\n", buf[3] + (buf[4] >> 8));
	printf("    Payload:      %s\n", buf[0] ? buf + 5 : "<RHMP>");
	printf("    Checksum:     0x%X\n", calc_ichksum(buf, len));

	if (!buf[0])
		print_RHMP(buf + 5);
}

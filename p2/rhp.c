/* RHP encapsulator/de-encapsulator for Project 2 in ECE312 Winter 2018/19
 *
 * Puts a payload read from STDIN inside an RHP packet and writes it to STDOUT.
 * If you try to give it a payload that is bigger than (BUFLEN - 7), it will be
 * truncated, and if the provided payload is an even number of octets a null
 * char will be added to the end of it.
 *
 * As with udp.c, it is designed to be used with UNIX pipes.  This program can
 * take 0 or 1 arguments.  If none are given, it will try to de-encapsulate the
 * given payload.  If there is one starting with an 's', then it will
 * encapsulate the given payload.  For the RHP packet to be of the type control
 * message, give a '1' after the 's', for it to be of type RHMP, use a '0'.
 *
 * Example usage sending the string "hello" to a server and then echoing the
 * response back to the terminal:
 *
 * # echo "hello" | ./rhp s1 | ./udp | ./rhp | cat
 *
 * Note 0:  Packet details print to stderr so it doesn't interfere with all the
 *          piping that's going on
 * Note 1:  Any packets which fail the checksum will be discarded and NULL will
 *          be written to STDOUT for the next thing to handle
 *
 * Created by Wesley Van Pelt, Feb 2019
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#define BUFLEN   2048
#define DST_PORT 105
#define SRC_PORT 3289


// Calculates the internet checksum on sum buffer
uint16_t calc_ichksum(uint8_t* buffer, int len)
{
	uint32_t sum   = 0;
	uint16_t sum16 = 0;
	int i;

	// First summing
	uint16_t *buf = (uint16_t *) buffer;
	for (i = 0; i < len / 2; sum += buf[i++]);

	// Check for the extra odd byte and add it on
	if (i * 2 < len)
		sum += ((uint16_t) buffer[i * 2]) << 8;

	// Add those bits over 16 back into the sum, invert, and return
	return ~((sum & 0xFFFF) + (sum >> 16));
}


// Pipe stuff from STDIN down the line encapsulated in RHP
void snd(char type)
{
	uint8_t  buf[BUFLEN];
	uint16_t chk;
	int      i;

	int b = read(STDIN_FILENO, buf, BUFLEN);

	// Make the data in the buffer so that there's an even number of octets,
	// like in the spec
	if (b % 2 == 0 && b < BUFLEN)
		buf[b++] = 0x00;

	// Move all the stuff over by 5 bytes to make room for header
	if (b > BUFLEN - 7)  // 5 bytes for header and 2 for checksum
		b = BUFLEN - 7;
	for (i = 0; i < b; i++)
		buf[i + 5] = buf[i];

	// Figure out and populate header
	if (type == '0') {
		buf[0] = 0x00;
		buf[1] = DST_PORT & 0xFF;
		buf[2] = DST_PORT >> 8;
	} else {
		buf[0] = 0x01;
		buf[1] = b & 0xFF;
		buf[2] = b >> 8;
	}
	buf[3] = SRC_PORT & 0xFF;
	buf[4] = SRC_PORT >> 8;

	// Fill in the checksum
	chk        = calc_ichksum(buf, b + 5);
	buf[b + 5] = chk & 0xFF;
	buf[b + 6] = chk >> 8;

	// Send the encapsulated data down the pipe
	write(STDOUT_FILENO, buf, b + 7);
}


// Strips off RHP stuff and pipes it down the line
void rcv()
{
	uint16_t chk;
	uint8_t  buf[BUFLEN];

	int b = read(STDIN_FILENO, buf, BUFLEN);

	// Verify checksum
	if (chk = calc_ichksum(buf, b)) {
		write(STDOUT_FILENO, NULL, 0);
		return;
	}

	fprintf(stderr, "\nRHP:\n");
	fprintf(stderr, "    Type:         %d\n", buf[0]);
	fprintf(stderr, "    Des Port/Len: %d\n", buf[1] + (buf[2] >> 8));
	fprintf(stderr, "    Src Port:     %d\n", buf[3] + (buf[4] >> 8));
	fprintf(stderr, "    Payload:      %s\n", buf[0] ? buf + 5 : "<RHMP>");
	fprintf(stderr, "    Checksum:     0x%X\n", chk);

	write(STDOUT_FILENO, buf + 5, b - 7);
}


int main(int argc, char** argv)
{
	if (argc > 1 && argv[1][0] == 's')
		snd(argv[1][1]);
	else
		rcv();

	return 0;
}

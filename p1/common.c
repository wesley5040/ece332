// Common functions for the useless messaging program
// ECE312
// Wesley Van Pelt  2018-12-09

#include "common.h"

void error(char* msg)
{
	perror(msg);
	exit(1);
}


void getInput(char* buf, int buf_l) {
	int i;

	if (fgets(buf, buf_l, stdin) == NULL)
		error("Input parse error");

	// Remove newlines from input
	for (i = 0; i < buf_l; i++) {
		if (buf[i] == '\n') {
			buf[i] = 0;
			break;
		}
	}
}


void* recv_loop(void* unused)
{
	int ret;
	char buf[BUF_LEN];

	while ((ret = recv((int) sock, buf, BUF_LEN, 0)) > 0) {
		if (!strncmp(buf, EOT, strlen(EOT)))
			break;
		printf("\n%s> %s\n%s> ", other_name, buf, self_name);
		fflush(stdout);  // Ahhh, gotta love threads...
	}
	if (ret < 0)
		printf("\nrecv err\n");

	sig_handler(SIGLOCAL);
}


void repl()
{
	char buf[BUF_LEN];
	pthread_t rThread;

	// Make thread to listen for incoming messages
	if (pthread_create(&rThread, NULL, recv_loop, NULL))
		error("Error creating thread");

	for ever {
		printf("%s> ", self_name);
		getInput(buf, BUF_LEN);
		send(sock, buf, strlen(buf), 0);
	}
}

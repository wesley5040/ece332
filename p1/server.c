// Server for the useless messaging program
// ECE312
// Wesley Van Pelt  2018-12-09

#include "common.h"

#define CLNT_ADDR_SL 100

int bsock;


void sig_handler(int signo)
{
	int x;
	if (signo != SIGINT && signo != SIGLOCAL)
		return;

	// This signals to the client to close the connection first to avoid the
	// wait time on starting the server again.  This method will be called
	// again when the client closes their socket, so we don't need to close
	// our sockets now.
	if (signo == SIGINT) {
		send(sock, EOT, strlen(EOT), 0);
		return;
	}

	close(sock);
	close(bsock);
	printf("\nConnection closed.\n");
	exit(0);
}


void main(int argc, char *argv[])
{
	int    clnt_l;
	char   clnt_addr_s[CLNT_ADDR_SL];
	struct sockaddr_in svr_addr, clnt_addr;

	printf("This is the useless messaging program.\n");
	printf("Use ^C to exit.\n");

	// Register SIGINT
	if (signal(SIGINT, sig_handler) == SIG_ERR)
		error("Could not register sigint\n");

	// Create the socket
	if ((bsock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		error("Socket creation failed D:< \n");

	// Initialize the endpoint structure
	memset(&svr_addr, 0, sizeof(svr_addr));
	svr_addr.sin_family      = AF_INET;
	svr_addr.sin_port        = htons(PORT);
	svr_addr.sin_addr.s_addr = INADDR_ANY;

	// Bind the socket
	if (bind(bsock, (struct sockaddr *) &svr_addr, sizeof(svr_addr)) < 0)
		error("Binding error");

	// Get username
	printf("Your username (between 2 and %d chars)> ", NAME_LEN - 1);
	getInput(self_name, NAME_LEN);

	// Wait for client
	printf("Waiting for client...\n");
	listen(bsock, 5);
	clnt_l = sizeof(clnt_addr);
	sock   = accept(bsock, (struct sockaddr *) &clnt_addr, &clnt_l);
	inet_ntop(AF_INET, &(clnt_addr.sin_addr), clnt_addr_s, CLNT_ADDR_SL);

	// Finish up username stuff
	recv(sock, other_name, NAME_LEN, 0);
	send(sock, self_name, strlen(self_name), 0);
	printf("Connection accepted from %s...%s\n", clnt_addr_s, other_name);

	// Start doing the actual messaging thing
	repl();
}

// Client for the useless messaging program
// ECE312
// Wesley Van Pelt  2018-12-09

#include "common.h"

#define SERVER_ADDR "172.16.105.10"


static unsigned long resolveName(const char* name)
{
	struct hostent *host;
	if ((host = gethostbyname(name)) == NULL)
		error("gethostbyname() failed");
	return *((unsigned long*)host->h_addr_list[0]);
}


void sig_handler(int signo)
{
	if (signo != SIGINT && signo != SIGLOCAL)
		return;

	close(sock);
	printf("\nConnection closed.\n");
	exit(0);
}


void main(int argc, char *argv[])
{
	struct sockaddr_in svr_addr;

	printf("This is the useless messaging program.\n");
	printf("Recompile to change the server address.\n");
	printf("Use ^C to exit.\n");

	// Register SIGINT
	if (signal(SIGINT, sig_handler) == SIG_ERR)
		error("Could not register sigint\n");

	// Create the socket
	if ((sock = socket(PF_INET, SOCK_STREAM, 0)) < 0)
		error("Socket creation failed D:< \n");

	// Initialize the endpoint structure
	memset(&svr_addr, 0, sizeof(svr_addr));
	svr_addr.sin_family      = AF_INET;
	svr_addr.sin_port        = htons(PORT);
	svr_addr.sin_addr.s_addr = resolveName(SERVER_ADDR);

	// Connect to server
	if (connect(sock, (struct sockaddr *) &svr_addr, sizeof(svr_addr)) < 0)
		error("Error connecting to server\n");

	// Get usernames together
	printf("Your username (between 2 and %d chars)> ", NAME_LEN - 1);
	getInput(self_name, NAME_LEN);
	send(sock, self_name, strlen(self_name), 0);
	recv(sock, other_name, NAME_LEN, 0);

	// Start doing the actual messaging thing
	repl();
}

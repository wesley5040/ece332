// Common header file for the useless messaging program
// ECE312
// Wesley Van Pelt  2018-12-09

#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>

#define PORT     4584
#define BUF_LEN  256
#define NAME_LEN 9
#define SIGLOCAL 0      // 0 is not bound to any SIG according to the man pages
#define EOT      "\x04" // 0x4 is ASCII for EOT (End Of Transmission)
#define ever     (;;)   // For the memes


void  error(char* msg);
void  getInput(char* buf, int buf_l);
void* recv_loop(void* sockit);
void  repl();

char self_name[NAME_LEN];
char other_name[NAME_LEN];
int  sock;


// These functions need to be made in the client and server files
void sig_handler(int signo);

#!/usr/bin/python

# Simulates a T1 synchronization in Python for PS4
# Part A
# Wesley Van Pelt  2018-01-09

import random

run_count = 10000

total = 0
for _ in range(run_count):
	count = 1  # Starting with 1 since this is math and not good stuff
	last_val = random.randint(0, 1)
	while True:
		new_val = random.randint(0, 1)
		count += 1  # Incrementing here to be consistent with in class
		if last_val == new_val:
			# Violation found
			break
		last_val = new_val
	total += count

print float(total)/run_count  # Using float() so Python doesn't do int division

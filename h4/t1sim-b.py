#!/usr/bin/python

# Simulates a T1 synchronization in Python for PS4
# Part B
# Wesley Van Pelt  2018-01-15

import random

run_count = 10000

total = 0
for _ in range(run_count):
	seq_bit  = random.randint(0, 192)
	count    = 0
	new_val  = random.randint(0, 1)
	last_val = new_val
	while (count % 193 != seq_bit):
		# if last_val != new_val, then it's good so far
		count   += 193 if (last_val != new_val) else 1
		last_val = new_val
		new_val  = random.randint(0, 1)
	total += count + 1739  # 1739 = 193*9+1+1, the extra +1 from 0 indexing

print float(total)/run_count  # Using float() so Python doesn't do int division
